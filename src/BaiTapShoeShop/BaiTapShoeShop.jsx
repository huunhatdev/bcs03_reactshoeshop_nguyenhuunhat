import React, { Component } from "react";
import data from "./dataShoeShop.json";
import ModalComponent from "./ModalComponent";
import ProductList from "./ProductList";

export default class BaiTapShoeShop extends Component {
  state = {
    data: data,
    cart: [],
  };

  handleAddCart = (product) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((e) => {
      return e.id === product.id;
    });
    if (index === -1) {
      cloneCart.push({ ...product, quantity: 1 });
    } else {
      cloneCart[index].quantity += 1;
    }

    this.setState({
      cart: cloneCart,
    });
  };
  handleSubCart = (i) => {
    let cloneCart = [...this.state.cart];
    let quant = cloneCart[i].quantity;
    if (quant > 1) {
      cloneCart[i].quantity -= 1;
    } else {
      cloneCart.splice(i, 1);
    }

    this.setState({
      cart: cloneCart,
    });
  };

  handleRemove = (i) => {
    let cloneCart = [...this.state.cart];
    cloneCart.splice(i, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  countProductInCart = () => {
    return this.state.cart.length;
  };

  render() {
    return (
      <div>
        <ProductList
          shoeList={this.state.data}
          handleAddCart={this.handleAddCart}
          countProductInCart={this.countProductInCart}
        />
        <ModalComponent
          data={this.state.cart}
          handleRemove={this.handleRemove}
          addProduct={this.handleAddCart}
          subProduct={this.handleSubCart}
        />
      </div>
    );
  }
}
