import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    let { shoeList, handleAddCart, countProductInCart } = this.props;

    return (
      <>
        <h1 className="bg-dark text-white py-2">Shoe Shop</h1>
        <div className="container">
          <div className="row justify-content-between">
            <p className="my-2">
              Số đơn hàng trong giỏ: {countProductInCart()}
            </p>
            <button
              className="btn btn-success my-2"
              data-toggle="modal"
              data-target="#modelId"
            >
              Giỏ hàng
            </button>
          </div>
          <div className="row">
            {shoeList.map((e, i) => {
              return (
                <ProductItem key={i} item={e} handleAddCart={handleAddCart} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}
