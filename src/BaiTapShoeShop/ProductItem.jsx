import React, { Component } from "react";
import "./style.css";

export default class ProductItem extends Component {
  render() {
    let { item } = this.props;
    return (
      <div className="col-4 p-2">
        <section className="card w-100">
          <div className="product-image">
            <img
              src={item.image}
              alt="OFF-white Red Edition"
              draggable="false"
            />
          </div>
          <div className="product-info">
            <h2>{item.name}</h2>
            <p>{item.shortDescription}</p>
            <div className="price">${item.price}</div>
          </div>
          <div className="btn">
            <button
              className="btn btn-warning text-white"
              onClick={() => {
                this.props.handleAddCart(item);
              }}
            >
              Add to cart
            </button>
          </div>
        </section>
      </div>
    );
  }
}
