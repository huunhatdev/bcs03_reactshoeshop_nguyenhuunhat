import React, { Component } from "react";

export default class ModalComponent extends Component {
  render() {
    let { data, handleRemove, addProduct, subProduct } = this.props;

    return (
      <div>
        <div
          className="modal  fade"
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div
            className="modal-dialog"
            style={{ maxWidth: "60%" }}
            role="document"
          >
            <div className="modal-content ">
              <div className="modal-header">
                <h5 className="modal-title">Giỏ hàng</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Image</th>
                      <th scope="col">Name</th>
                      <th scope="col">Price</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Total</th>
                      <th scope="col">Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((e, i) => {
                      return (
                        <tr>
                          <th scope="row">{i + 1}</th>
                          <td>
                            <img
                              src={e.image}
                              style={{ height: "8rem" }}
                              alt=""
                            />{" "}
                          </td>
                          <td>{e.name}</td>
                          <td>{e.price}</td>
                          <td>
                            <i
                              class="bi bi-dash"
                              onClick={() => {
                                subProduct(i);
                              }}
                            ></i>
                            {e?.quantity}
                            <i
                              class="bi bi-plus"
                              onClick={() => {
                                addProduct(e);
                              }}
                            ></i>
                          </td>
                          <td>{e.price * e?.quantity}</td>
                          <td>
                            <i
                              class="bi bi-trash3 text-danger"
                              onClick={() => {
                                handleRemove(i);
                              }}
                            ></i>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-success">
                  Payment
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
